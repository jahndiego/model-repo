---
MorpheusModelID: M8638

contributors: [J. Starruß]

title: Circle Dance
date: "2020-05-08T00:00:00+02:00"
lastmod: "2020-11-09T19:22:00+01:00"
---

>Demonstration of dynamically regulated cell-cell adhesion as a function of changing cell properties