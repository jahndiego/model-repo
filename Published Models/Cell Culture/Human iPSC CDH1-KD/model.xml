<?xml version='1.0'?>
<MorpheusModel version="4">
    <Description>
        <Title>Libby2019</Title>
        <Details>Full title:        Human iPSC CDH1-KD : Wildtype Colony Patterning.
Date: 	30.06.2021
Authors: 	A. R.G. Libby, D. Briers, I. Haghighi, D. A. Joy, B. R. Conklin, C. Belta, T. C. McDevitt
Curators: 	Lutz Brusch (lutz.brusch@tu-dresden.de)
Software: 	Morpheus (open-source), download from https://morpheus.gitlab.io
ModelID: 	https://identifiers.org/morpheus/M7672
Reference:    This model is described in the peer-reviewed publication 
	A. R.G. Libby, D. Briers, I. Haghighi, D. A. Joy, B. R. Conklin, C. Belta, T. C. McDevitt. Automated Design of Pluripotent Stem Cell Self-Organization. Cell Systems 9(5): 483–495, 2019. (cf. Video S4 at https://www.sciencedirect.com/science/article/pii/S2405471219303849?#mmc6)
</Details>
    </Description>
    <Global>
        <Variable symbol="ct1_adhesion" value="-100"/>
        <Variable symbol="ct2_adhesion" value="-100"/>
        <Variable symbol="ct1_ct2" value="max(ct1_adhesion,ct2_adhesion)"/>
        <Constant symbol="ctall_medium_adhesion_init" value="0"/>
        <Variable symbol="ctall_medium_adhesion" value="ctall_medium_adhesion_init"/>
        <Variable name="time_of_knockout_initiation_ct1_adhesion" symbol="ct1_perturbation_time" value="-144"/>
        <Constant symbol="adhesion_init_ct1" value="-100"/>
        <Variable symbol="ct1_adhesion_weak" value="-100"/>
        <Variable symbol="ct1_membraneElasticity_init" value="1.12"/>
        <Variable symbol="ct1_membraneElasticity" value="1.12"/>
        <Variable symbol="ct1_membraneElasticity_final" value="1.12"/>
        <Variable symbol="ct1_membraneElasticity_edge_init" value="1.32"/>
        <Variable symbol="ct1_membraneElasticity_edge" value="1.32"/>
        <Variable symbol="ct1_membraneElasticity_final_edge" value="1.32"/>
        <Variable symbol="ct1_k_half_hours" value="48.28"/>
        <Variable name="" symbol="ct1_hill_n" value="5.0"/>
        <Variable symbol="ct1_area_init" value="137"/>
        <Variable name="" symbol="ct1_area" value="137"/>
        <Variable name="" symbol="ct1_area_final" value="137"/>
        <Variable symbol="ct1_area_final_sd" value="34.03"/>
        <Variable symbol="ct1_area_edge_init" value="177"/>
        <Variable name="" symbol="ct1_area_edge" value="177"/>
        <Variable name="" symbol="ct1_area_edge_final" value="177"/>
        <Variable symbol="ct1_area_edge_final_sd" value="49.66"/>
        <Variable symbol="ct1_generation_time_init" value="20"/>
        <Variable symbol="ct1_generation_time_final" value="20"/>
        <Variable symbol="ct1_generation_time" value="20"/>
        <Variable symbol="ct1_membrane_str_init" value="0.5"/>
        <Variable symbol="ct1_membrane_str_final" value="0.5"/>
        <Variable symbol="ct1_membrane_str" value="0.5"/>
        <Variable symbol="ct1_persistent_motion_str_init" value="9"/>
        <Variable symbol="ct1_persistent_motion_str_final" value="9"/>
        <Variable symbol="ct1_persistent_motion" value="9"/>
        <Variable name="time_of_knockout_initiation_ct2_adhesion" symbol="ct2_perturbation_time" value="0"/>
        <Constant symbol="adhesion_init_ct2" value="-100"/>
        <Variable symbol="ct2_adhesion_weak" value="-70"/>
        <Variable symbol="ct2_membraneElasticity_init" value="1.12"/>
        <Variable symbol="ct2_membraneElasticity" value="1.12"/>
        <Variable symbol="ct2_membraneElasticity_final" value="1.1"/>
        <Variable symbol="ct2_membraneElasticity_edge_init" value="1.32"/>
        <Variable symbol="ct2_membraneElasticity_edge" value="1.32"/>
        <Variable symbol="ct2_membraneElasticity_final_edge" value="1.23"/>
        <Variable symbol="ct2_k_half_hours" value="48.28"/>
        <Variable symbol="ct2_hill_n" value="5.0"/>
        <Variable symbol="ct2_area_init" value="137"/>
        <Variable symbol="ct2_area" value="137"/>
        <Variable symbol="ct2_area_final_sd" value="51.78"/>
        <Variable symbol="ct2_area_final" value="123"/>
        <Variable symbol="ct2_area_edge_init" value="177"/>
        <Variable symbol="ct2_area_edge" value="177"/>
        <Variable symbol="ct2_area_edge_final" value="223"/>
        <Variable symbol="ct2_area_edge_final_sd" value="57.96"/>
        <Variable symbol="ct2_generation_time_init" value="20"/>
        <Variable symbol="ct2_generation_time_final" value="18"/>
        <Variable symbol="ct2_generation_time" value="20"/>
        <Variable symbol="ct2_membrane_str_init" value="0.5"/>
        <Variable symbol="ct2_membrane_str_final" value="0.5"/>
        <Variable symbol="ct2_membrane_str" value="0.5"/>
        <Variable symbol="ct2_persistent_motion_str_init" value="9"/>
        <Variable symbol="ct2_persistent_motion_str_final" value="9"/>
        <Variable symbol="ct2_persistent_motion" value="9"/>
        <System solver="Euler [fixed, O(1)]" name="ct1_mechanics_updater" time-step="0.1">
            <Rule symbol-ref="ct1_adhesion">
                <Expression>if(time &lt; burn_in_time, adhesion_init_ct1,
  if(time > ct1_perturbation_time
  and ct1_adhesion >= adhesion_init_ct1
  and ct1_adhesion &lt;= ct1_adhesion_weak,
    (1.0 / (1.0 + (pow(((time-ct1_perturbation_time)/ct1_k_half_hours),ct1_hill_n))))* (adhesion_init_ct1-ct1_adhesion_weak) + ct1_adhesion_weak,
  if(time &lt;= ct1_perturbation_time,adhesion_init_ct1,ct1_adhesion_weak)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct1_membraneElasticity">
                <Expression>if(time &lt; burn_in_time,ct1_membraneElasticity_init,
  if(time > ct1_perturbation_time 
  and ct1_membraneElasticity >= ct1_membraneElasticity_init
  and ct1_membraneElasticity &lt;= ct1_membraneElasticity_final,
    (1.0 / (1.0 + (pow(((time-ct1_perturbation_time)/ct1_k_half_hours),ct1_hill_n))))*(ct1_membraneElasticity_init-ct1_membraneElasticity_final) + ct1_membraneElasticity_final,
  if(time &lt;= ct1_perturbation_time,ct1_membraneElasticity_init,ct1_membraneElasticity_final)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct1_membraneElasticity_edge">
                <Expression>if(time &lt; burn_in_time,ct1_membraneElasticity_edge_init,
  if(time > ct1_perturbation_time 
    and ct1_membraneElasticity_edge >= ct1_membraneElasticity_edge_init
    and ct1_membraneElasticity_edge &lt;= ct1_membraneElasticity_final_edge,
    (1.0 / (1.0 + (pow(((time-ct1_perturbation_time)/ct1_k_half_hours),ct1_hill_n))))*(ct1_membraneElasticity_edge_init-ct1_membraneElasticity_final_edge) + ct1_membraneElasticity_final_edge,
  if(time &lt;= ct1_perturbation_time,ct1_membraneElasticity_edge_init,ct1_membraneElasticity_final_edge)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct1_area">
                <Expression>if(time >burn_in_time,ct1_area_init,
 if( (ct1_area_init &lt; ct1_area_final) 
     and ct1_area &lt; ct1_area_final,
ct1_area_init + 
(ct1_area_final-ct1_area_init)*
((time-ct1_perturbation_time)/96),
ct1_area_final)
)</Expression>
            </Rule>
            <Rule symbol-ref="ct1_area_edge">
                <Expression>if(time &lt; burn_in_time,ct1_area_edge_init,
  if((ct1_area_edge_init &lt; ct1_area_edge_final) 
     and ct1_area_edge &lt; ct1_area_edge_final,
ct1_area_edge_init + 
(ct1_area_edge_final-ct1_area_edge_init)*
((time-ct1_perturbation_time)/96),
ct1_area_edge_final)
)
                </Expression>
            </Rule>
            <Rule symbol-ref="ct1_generation_time">
                <Expression>if(time &lt; burn_in_time, ct1_generation_time_init,
  if(time > ct1_perturbation_time,
    (1.0 / (1.0 + (pow(((time-ct1_perturbation_time)/ct1_k_half_hours),ct1_hill_n))))* (ct1_generation_time_init-ct1_generation_time_final) + ct1_generation_time_final,
  if(time &lt;= ct1_perturbation_time,ct1_generation_time_init,ct1_generation_time_final)
))
</Expression>
            </Rule>
            <Rule symbol-ref="ct1_membrane_str">
                <Expression>if(time &lt; burn_in_time, ct1_membrane_str_init,
  if(time > ct1_perturbation_time
  and ct1_membrane_str >= ct1_membrane_str_init
  and ct1_membrane_str &lt;= ct1_membrane_str_final,
    (1.0 / (1.0 + (pow(((time-ct1_perturbation_time)/ct1_k_half_hours),ct1_hill_n))))* (ct1_membrane_str_init-ct1_membrane_str_final) + ct1_membrane_str_final,
  if(time &lt;= ct1_perturbation_time,ct1_membrane_str_init,ct1_membrane_str_final)
))</Expression>
            </Rule>
        </System>
        <System solver="Euler [fixed, O(1)]" name="ct2_mechanics_updater" time-step="0.1">
            <Rule symbol-ref="ct2_adhesion">
                <Expression>if(time &lt; burn_in_time,adhesion_init_ct2,
  if(time > ct2_perturbation_time 
  and ct2_adhesion >= adhesion_init_ct2
  and ct2_adhesion &lt;= ct2_adhesion_weak,
  (1.0 / (1.0 + (pow(((time-ct2_perturbation_time)/ct2_k_half_hours),ct2_hill_n))))*(adhesion_init_ct2-ct2_adhesion_weak) + ct2_adhesion_weak,
  if(time &lt;= ct2_perturbation_time,adhesion_init_ct2,ct2_adhesion_weak)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct2_membraneElasticity">
                <Expression>if(time &lt; burn_in_time,ct2_membraneElasticity_init,
  if(time > ct2_perturbation_time 
  and ct2_membraneElasticity >= ct2_membraneElasticity_init
  and ct2_membraneElasticity &lt;= ct2_membraneElasticity_final,
    (1.0 / (1.0 + (pow(((time-ct2_perturbation_time)/ct2_k_half_hours),ct2_hill_n))))*
(ct2_membraneElasticity_init-ct2_membraneElasticity_final) + ct2_membraneElasticity_final,
  if(time &lt;= ct2_perturbation_time,ct2_membraneElasticity_init,ct2_membraneElasticity_final)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct2_membraneElasticity_edge">
                <Expression>if(time &lt; burn_in_time,ct2_membraneElasticity_edge_init,
  if(time > ct2_perturbation_time 
  and ct2_membraneElasticity_edge >= ct2_membraneElasticity_edge_init
  and ct2_membraneElasticity_edge &lt;= ct2_membraneElasticity_final_edge,
    (1.0 / (1.0 + (pow(((time-ct2_perturbation_time)/ct2_k_half_hours),ct2_hill_n))))*(ct2_membraneElasticity_edge_init-ct2_membraneElasticity_final_edge) + ct2_membraneElasticity_final_edge,
  if(time &lt;= ct2_perturbation_time,ct2_membraneElasticity_edge_init,ct2_membraneElasticity_final_edge)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct2_area">
                <Expression>if(time &lt; burn_in_time,ct2_area_init,
  if( (ct2_area_init &lt; ct2_area_final)
    and ct2_area &lt; ct2_area_final,
    ct2_area_init + (ct2_area_final-ct2_area_init)*
    ((time-ct2_perturbation_time)/96),
  ct2_area_final)
)             </Expression>
            </Rule>
            <Rule symbol-ref="ct2_area_edge">
                <Expression>if(time &lt; burn_in_time,ct2_area_edge_init,
  if( (ct2_area_edge_init &lt; ct2_area_edge_final)
     and ct2_area_edge &lt; ct2_area_edge_final,
    ct2_area_edge_init + 
    (ct2_area_edge_final-ct2_area_edge_init)*
    ((time-ct2_perturbation_time)/96),
  ct2_area_edge_final)
)</Expression>
            </Rule>
            <Rule symbol-ref="ct2_generation_time">
                <Expression>if(time &lt; burn_in_time, ct2_generation_time_init,
  if(time > ct2_perturbation_time,
    (1.0 / (1.0 + (pow(((time-ct2_perturbation_time)/ct2_k_half_hours),ct2_hill_n))))* (ct2_generation_time_init-ct2_generation_time_final) + ct2_generation_time_final,
  if(time &lt;= ct2_perturbation_time,ct2_generation_time_init,ct2_generation_time_final)
))
</Expression>
            </Rule>
            <Rule symbol-ref="ct2_membrane_str">
                <Expression>if(time &lt; burn_in_time, ct2_membrane_str_init,
  if(time > ct2_perturbation_time
  and ct2_membrane_str >= ct2_membrane_str_init
  and ct2_membrane_str &lt;= ct2_membrane_str_final,
    (1.0 / (1.0 + (pow(((time-ct2_perturbation_time)/ct1_k_half_hours),ct2_hill_n))))* (ct2_membrane_str_init-ct2_membrane_str_final) + ct2_membrane_str_final,
  if(time &lt;= ct2_perturbation_time,ct2_membrane_str_init,ct2_membrane_str_final)
))</Expression>
            </Rule>
        </System>
        <System solver="Euler [fixed, O(1)]" name="adhesion_ct1_ct2" time-step="0.1">
            <Rule symbol-ref="ct1_ct2">
                <Expression>max(ct1_adhesion,ct2_adhesion)</Expression>
            </Rule>
        </System>
        <Constant symbol="wt_offset_x" value="0"/>
        <Constant symbol="wt_offset_y" value="0"/>
        <Constant symbol="inhibited_offset_x" value="0"/>
        <Constant symbol="inhibited_offset_y" value="0"/>
        <Field name="chemoattractant" symbol="U" value="0">
            <Diffusion rate="0"/>
        </Field>
        <!--    <Disabled>
        <System solver="Euler [fixed, O(1)]" time-step="1">
            <DiffEqn symbol-ref="U">
                <Expression>p-degp*U</Expression>
            </DiffEqn>
        </System>
    </Disabled>
-->
        <!--    <Disabled>
        <Constant symbol="cell_density" value="0.003"/>
    </Disabled>
-->
        <!--    <Disabled>
        <Field symbol="protrusionMemory" value="0">
            <Diffusion rate="0.0"/>
        </Field>
    </Disabled>
-->
        <Variable symbol="num_cells_ct2" value="50"/>
        <Variable symbol="num_cells_ct1" value="100-num_cells_ct2"/>
        <Variable name="percent_membrane_bordeing_media" symbol="avgMediaNeighborLength" value="1"/>
        <Variable symbol="burn_in_time" value="5"/>
    </Global>
    <Space>
        <Lattice class="square">
            <Size symbol="lattice" value="1200,1200,0"/>
            <BoundaryConditions>
                <Condition boundary="x" type="constant"/>
                <Condition boundary="y" type="constant"/>
            </BoundaryConditions>
            <Neighborhood>
                <Order>4</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime symbol="hours" value="120"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="0"/>
    </Time>
    <CellTypes>
        <CellType name="ct1" class="biological">
            <Property name="divisions" symbol="d" value="0"/>
            <Property name="color" symbol="c" value="0"/>
            <Property name="generationTime" symbol="dt" value="ct1_generation_time"/>
            <Property name="division_time_counter" symbol="dc" value="0.0"/>
            <Property name="cell (for counting)" symbol="cell" value="1.0"/>
            <Property symbol="cadherin" value="ct1_adhesion"/>
            <Property symbol="ct1_ct2_adhesion" value="ct1_ct2"/>
            <!--    <Disabled>
        <Property name="percentVolume2DaughterCell" symbol="percentDaughterVolume" value="0.5"/>
    </Disabled>
-->
            <!--    <Disabled>
        <Constant symbol="maxTargetArea" value="32.0"/>
    </Disabled>
-->
            <!--    <Disabled>
        <Property symbol="cellVolumeIncrement" value="0.5*maxTargetArea/dt"/>
    </Disabled>
-->
            <Property symbol="targetArea" value="ct1_area"/>
            <Property symbol="targetSurface" value="ct1_membraneElasticity"/>
            <VolumeConstraint target="targetArea" strength="1.0"/>
            <SurfaceConstraint target="targetSurface" mode="aspherity" strength="ct1_membrane_str"/>
            <PropertyVector name="direction_vector" symbol="dv" value="0.0, 0.0, 0.0"/>
            <PersistentMotion protrusion="true" decay-time="0.003" strength="9"/>
            <!--    <Disabled>
        <Event trigger="when true" name="partitionCell">
            <Condition>dc == (dt-1)</Condition>
            <Rule symbol-ref="percentDaughterVolume">
                <Expression>rand_uni(0.45,0.55)</Expression>
            </Rule>
        </Event>
    </Disabled>
-->
            <CellDivision daughterID="daughter" division-plane="minor">
                <Condition>dc >= dt</Condition>
                <Triggers name="">
                    <Rule symbol-ref="dc">
                        <Expression>0</Expression>
                    </Rule>
                    <!--    <Disabled>
        <Rule symbol-ref="targetArea">
            <Expression>if( daughter == 1, 
percentDaughterVolume*maxTargetArea, 
(1-percentDaughterVolume)*maxTargetArea )</Expression>
        </Rule>
    </Disabled>
-->
                </Triggers>
            </CellDivision>
            <System solver="Euler [fixed, O(1)]" name="cell_state_updater" time-step="1">
                <Rule symbol-ref="dc">
                    <Expression>dc+1</Expression>
                </Rule>
                <!--    <Disabled>
        <Rule symbol-ref="c">
            <Expression>if(isEdgeCell > 0,2,0)</Expression>
        </Rule>
    </Disabled>
-->
                <!--    <Disabled>
        <Rule symbol-ref="targetArea">
            <Expression>if((time-ct1_perturbation_time) > ct1_k_half_hours,
  if(isEdgeCell > 0,
    cell_area_edge_final, 
    cell_area_final),
  if(isEdgeCell > 0,
    cell_area_edge,
    cell_area)  
)</Expression>
        </Rule>
    </Disabled>
-->
                <Rule symbol-ref="targetSurface">
                    <Expression>if(isEdgeCell > 0,
ct1_membraneElasticity_edge,
ct1_membraneElasticity)</Expression>
                </Rule>
            </System>
            <!--    <Disabled>
        <PropertyVector symbol="displacement" value=""/>
    </Disabled>
-->
            <!--    <Disabled>
        <MotilityReporter time-step="1">
            <Displacement symbol-ref="displacement"/>
        </MotilityReporter>
    </Disabled>
-->
            <Property symbol="p" value="10"/>
            <Chemotaxis contact-inhibition="true" field="U" strength="3" retraction="true"/>
            <Constant symbol="degp" value="0.9"/>
            <!--    <Disabled>
        <Protrusion field="protrusionMemory" strength="1" maximum="10"/>
    </Disabled>
-->
            <Property symbol="isEdgeCell" value="0"/>
            <NeighborhoodReporter>
                <Input scaling="cell" value="cell.type == celltype.medium.id"/>
                <Output mapping="sum" symbol-ref="isEdgeCell"/>
            </NeighborhoodReporter>
            <Property symbol="ctall_medium_adhesion" value="ctall_medium_adhesion_init"/>
            <Property symbol="cell_area_final" value="0"/>
            <Property symbol="cell_area_edge_final" value="0"/>
        </CellType>
        <CellType name="ct2" class="biological">
            <Property name="divisions" symbol="d" value="0"/>
            <Property name="color" symbol="c" value="1"/>
            <Property name="cell (for counting)" symbol="cell" value="1.0"/>
            <Property name="generationTime" symbol="dt" value="ct2_generation_time"/>
            <Property name="division_time_counter" symbol="dc" value="0.0"/>
            <Property name="global_cadherin_inhibitedpop" symbol="cadherin" value="ct2_adhesion"/>
            <Property symbol="ct1_ct2_adhesion" value="ct1_ct2"/>
            <Property symbol="ctall_medium_adhesion" value="ctall_medium_adhesion_init"/>
            <Property symbol="avgLikeNeighborCount" value="0"/>
            <Property symbol="avgLikeNeighborLength" value="0"/>
            <Property symbol="targetArea" value="ct2_area"/>
            <Property symbol="targetSurface" value="ct2_membraneElasticity"/>
            <VolumeConstraint target="targetArea" strength="1"/>
            <SurfaceConstraint target="targetSurface" mode="aspherity" strength="ct2_membrane_str"/>
            <PropertyVector name="direction_vector" symbol="dv" value="0.0, 0.0, 0.0"/>
            <PersistentMotion protrusion="true" decay-time="0.003" strength="9"/>
            <CellDivision daughterID="daughter" division-plane="major">
                <Condition>dc >= dt</Condition>
                <Triggers name="">
                    <Rule symbol-ref="dc">
                        <Expression>0</Expression>
                    </Rule>
                    <!--    <Disabled>
        <Rule symbol-ref="targetArea">
            <Expression>if( daughter == 1, 
percentDaughterVolume*maxTargetArea, 
(1-percentDaughterVolume)*maxTargetArea )</Expression>
        </Rule>
    </Disabled>
-->
                </Triggers>
            </CellDivision>
            <System solver="Euler [fixed, O(1)]" name="cell_state_updater" time-step="1">
                <Rule symbol-ref="dc">
                    <Expression>dc+1</Expression>
                </Rule>
                <!--    <Disabled>
        <Rule symbol-ref="c">
            <Expression>if(isEdgeCell > 0,2,1)</Expression>
        </Rule>
    </Disabled>
-->
                <Rule name="CellGrowth" symbol-ref="targetArea">
                    <Expression>if(isEdgeCell > 0,ct2_area_edge,ct2_area)</Expression>
                </Rule>
                <Rule symbol-ref="targetSurface">
                    <Expression>if(isEdgeCell > 0,ct2_membraneElasticity_edge,ct2_membraneElasticity)</Expression>
                </Rule>
                <!--    <Disabled>
        <Rule name="peripheral_cell_migration" symbol-ref="ctall_medium_adhesion">
            <Expression>if(avgMediaNeighborLength > 0.35,max(cadherin,ct1_ct2_adhesion)/2,0)</Expression>
        </Rule>
    </Disabled>
-->
            </System>
            <!--    <Disabled>
        <PropertyVector symbol="displacement" value=""/>
    </Disabled>
-->
            <!--    <Disabled>
        <MotilityReporter time-step="1">
            <Displacement symbol-ref="displacement"/>
        </MotilityReporter>
    </Disabled>
-->
            <Property symbol="p" value="10"/>
            <Constant symbol="degp" value="0.9"/>
            <Chemotaxis contact-inhibition="true" field="U" strength="3" retraction="true"/>
            <!--    <Disabled>
        <Protrusion field="protrusionMemory" strength="1" maximum="10"/>
    </Disabled>
-->
            <Property symbol="isEdgeCell" value="0"/>
            <NeighborhoodReporter>
                <Input scaling="cell" value="cell.type == celltype.medium.id"/>
                <Output mapping="sum" symbol-ref="isEdgeCell"/>
            </NeighborhoodReporter>
            <Property symbol="cell_area_final" value="0"/>
            <Property symbol="cell_area_edge_final" value="0"/>
        </CellType>
        <!--    <Disabled>
        <CellType name="collagen" class="biological">
            <Property name="divisions" symbol="d" value="0"/>
            <Property name="color" symbol="c" value="2"/>
            <Property name="cell (for counting)" symbol="cell" value="1.0"/>
            <Disabled>
                <Property name="generationTime" symbol="dt" value="20"/>
            </Disabled>
            <Property name="division_time_counter" symbol="dc" value="0.0"/>
            <Property symbol="targetArea" value="10"/>
            <Property name="global_cadherin_inhibitedpop" symbol="cadherin" value="2"/>
            <Disabled>
                <Property symbol="avgLikeNeighborCount" value="0"/>
            </Disabled>
            <Disabled>
                <Property symbol="avgLikeNeighborLength" value="0"/>
            </Disabled>
            <VolumeConstraint target="targetArea" strength="1"/>
            <PropertyVector name="direction_vector" symbol="dv" value="0.0, 0.0, 0.0"/>
            <Disabled>
                <CellDivision daughterID="daughter" division-plane="major">
                    <Condition>dc >= dt</Condition>
                    <Triggers name="">
                        <Rule symbol-ref="dc">
                            <Expression>0</Expression>
                        </Rule>
                        <Disabled>
                            <Rule symbol-ref="targetArea">
                                <Expression>if( daughter == 1, 
percentDaughterVolume*maxTargetArea, 
(1-percentDaughterVolume)*maxTargetArea )</Expression>
                            </Rule>
                        </Disabled>
                    </Triggers>
                </CellDivision>
            </Disabled>
            <Property symbol="ct1_ct2_adhesion" value="2"/>
            <Property symbol="p" value="0.10"/>
            <Disabled>
                <Chemotaxis contact-inhibition="false" field="U" strength="c" retraction="true"/>
            </Disabled>
            <FreezeMotion>
                <Condition>time >=5</Condition>
            </FreezeMotion>
        </CellType>
    </Disabled>
-->
        <CellType name="medium" class="medium">
            <Property symbol="cadherin" value="ctall_medium_adhesion"/>
            <Property symbol="avgLikeNeighborCount" value="0"/>
            <Property symbol="avgLikeNeighborLength" value="0"/>
            <Property symbol="p" value="0"/>
            <!--    <Disabled>
        <Property symbol="dc" value="0"/>
    </Disabled>
-->
            <Constant symbol="degp" value="0.0"/>
        </CellType>
        <!--    <Disabled>
        <CellType name="sisterColony" class="biological">
            <Property name="divisions" symbol="d" value="0"/>
            <Property name="color" symbol="c" value="3"/>
            <Property name="cell (for counting)" symbol="cell" value="1.0"/>
            <Disabled>
                <Property name="generationTime" symbol="dt" value="20"/>
            </Disabled>
            <Property name="division_time_counter" symbol="dc" value="0.0"/>
            <Property symbol="targetArea" value="500"/>
            <Property name="global_cadherin_inhibitedpop" symbol="cadherin" value="2"/>
            <Disabled>
                <Property symbol="avgLikeNeighborCount" value="0"/>
            </Disabled>
            <Disabled>
                <Property symbol="avgLikeNeighborLength" value="0"/>
            </Disabled>
            <VolumeConstraint target="targetArea" strength="1"/>
            <PropertyVector name="direction_vector" symbol="dv" value="0.0, 0.0, 0.0"/>
            <Disabled>
                <CellDivision daughterID="daughter" division-plane="major">
                    <Condition>dc >= dt</Condition>
                    <Triggers name="">
                        <Rule symbol-ref="dc">
                            <Expression>0</Expression>
                        </Rule>
                        <Disabled>
                            <Rule symbol-ref="targetArea">
                                <Expression>if( daughter == 1, 
percentDaughterVolume*maxTargetArea, 
(1-percentDaughterVolume)*maxTargetArea )</Expression>
                            </Rule>
                        </Disabled>
                    </Triggers>
                </CellDivision>
            </Disabled>
            <Property symbol="ct1_ct2_adhesion" value="2"/>
            <Property symbol="p" value="500"/>
            <Disabled>
                <Chemotaxis contact-inhibition="false" field="U" strength="c" retraction="true"/>
            </Disabled>
            <Disabled>
                <FreezeMotion>
                    <Condition>time >=5</Condition>
                </FreezeMotion>
            </Disabled>
            <SurfaceConstraint target="0.5" mode="aspherity" strength="1"/>
        </CellType>
    </Disabled>
-->
    </CellTypes>
    <CPM>
        <Interaction default="0">
            <Contact type1="ct1" type2="medium" value="0">
                <HomophilicAdhesion strength="1" adhesive="ctall_medium_adhesion"/>
            </Contact>
            <Contact type1="ct2" type2="medium" value="0">
                <HomophilicAdhesion strength="1" adhesive="ctall_medium_adhesion"/>
            </Contact>
            <Contact type1="ct1" type2="ct1" value="0">
                <HomophilicAdhesion strength="1" adhesive="cadherin"/>
            </Contact>
            <Contact type1="ct1" type2="ct2" value="0">
                <HomophilicAdhesion strength="1" adhesive="ct1_ct2"/>
            </Contact>
            <Contact type1="ct2" type2="ct2" value="0">
                <HomophilicAdhesion strength="1" adhesive="cadherin"/>
            </Contact>
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="0.03"/>
            <Neighborhood>
                <Order>4</Order>
            </Neighborhood>
            <MetropolisKinetics yield="0.1" temperature="10"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population size="1" name="cell_type_1" type="ct1">
            <!--    <Disabled>
        <InitCircle mode="random" number-of-cells="75" random_displacement="0.3*lattice.x">
            <Dimensions center="lattice.x/2,lattice.y/2,0" radius="20"/>
        </InitCircle>
    </Disabled>
-->
            <InitProperty symbol-ref="dc">
                <Expression>rand_uni(0,dt)</Expression>
            </InitProperty>
            <InitRectangle mode="regular" random-offset="10" number-of-cells="num_cells_ct1">
                <Dimensions size="60,20,0" origin="(lattice.x/2)+wt_offset_x,(lattice.y/2)+wt_offset_y,0"/>
            </InitRectangle>
            <!--    <Disabled>
        <InitProperty symbol-ref="cell_area_final">
            <Expression>rand_norm(ct1_area_final,ct1_area_final_sd)</Expression>
        </InitProperty>
    </Disabled>
-->
            <!--    <Disabled>
        <InitProperty symbol-ref="cell_area_edge_final">
            <Expression>rand_norm(ct1_area_edge_final
    ,ct1_area_edge_final_sd)</Expression>
        </InitProperty>
    </Disabled>
-->
        </Population>
        <Population size="1" name="cell_type_2" type="ct2">
            <InitRectangle mode="regular" random-offset="10" number-of-cells="num_cells_ct2">
                <Dimensions size="60,20,0" origin="(lattice.x/2)+inhibited_offset_x,(lattice.y/2)+inhibited_offset_y,0"/>
            </InitRectangle>
            <!--    <Disabled>
        <InitCircle mode="random" number-of-cells="25" random_displacement="0.05*lattice.x">
            <Dimensions center="lattice.x/2, lattice.y/2,0" radius="10"/>
        </InitCircle>
    </Disabled>
-->
            <InitProperty symbol-ref="dc">
                <Expression>rand_uni(0,dt)</Expression>
            </InitProperty>
            <!--    <Disabled>
        <InitProperty symbol-ref="cell_area_final">
            <Expression>rand_norm(ct2_area_final,ct2_area_final_sd)</Expression>
        </InitProperty>
    </Disabled>
-->
            <!--    <Disabled>
        <InitProperty symbol-ref="cell_area_edge_final">
            <Expression>rand_norm(ct2_area_edge_final
    ,ct2_area_edge_final_sd)</Expression>
        </InitProperty>
    </Disabled>
-->
        </Population>
        <!--    <Disabled>
        <Population size="1" type="collagen">
            <InitRectangle mode="regular" number-of-cells="cell_density*lattice.*lattice.y">
                <Dimensions size="lattice.x/2,lattice.y,0" origin="0.0, 0.0, 0.0"/>
            </InitRectangle>
        </Population>
    </Disabled>
-->
        <!--    <Disabled>
        <Population size="1" type="sisterColony">
            <InitCircle mode="regular" number-of-cells="1">
                <Dimensions center="100,100,0" radius="1"/>
            </InitCircle>
        </Population>
    </Disabled>
-->
        <!--    <Disabled>
        <Population size="1" name="wildtype" type="ct1">
            <InitCircle mode="random" number-of-cells="75" random_displacement="0.3*lattice.x">
                <Dimensions center="50,50,0" radius="20"/>
            </InitCircle>
            <InitProperty symbol-ref="dc">
                <Expression>rand_uni(0,dt)</Expression>
            </InitProperty>
            <Disabled>
                <InitProperty symbol-ref="targetArea">
                    <Expression>rand_uni(16,32)</Expression>
                </InitProperty>
            </Disabled>
        </Population>
    </Disabled>
-->
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="1" decorate="false">
            <Terminal size="1200,1200,0" name="png"/>
            <Plot>
                <Cells min="0" max="3" value="c">
                    <ColorMap>
                        <Color color="dark-gray" value="0"/>
                        <Color color="blue" value="1"/>
                        <Color color="yellow" value="2"/>
                        <Color color="violet" value="3"/>
                    </ColorMap>
                </Cells>
                <!--    <Disabled>
        <Field min="0.0" max="100" surface="true" symbol-ref="U"/>
    </Disabled>
-->
            </Plot>
        </Gnuplotter>
        <!--    <Disabled>
        <HistogramLogger time-step="1" number-of-bins="20" normalized="true" minimum="14" maximum="32">
            <Column celltype="ct2" symbol-ref="targetArea"/>
            <Plot terminal="png" minimum="0.0" maximum="1"/>
        </HistogramLogger>
    </Disabled>
-->
        <!--    <Disabled>
        <Logger time-step="1">
            <Input>
                <Disabled>
                    <Symbol symbol-ref="cell.id"/>
                </Disabled>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
                <Symbol symbol-ref="cell.type"/>
                <Disabled>
                    <Symbol symbol-ref="avgMediaNeighborLength"/>
                </Disabled>
                <Disabled>
                    <Symbol symbol-ref="avgLikeNeighborLength"/>
                </Disabled>
            </Input>
            <Output>
                <TextOutput file-numbering="time" file-name="cell_positions"/>
            </Output>
        </Logger>
    </Disabled>
-->
        <Logger time-step="1">
            <Input>
                <Symbol symbol-ref="ct1_adhesion"/>
                <Symbol symbol-ref="ct1_membrane_str"/>
                <Symbol symbol-ref="ct1_area"/>
                <Symbol symbol-ref="ct1_area_edge"/>
                <Symbol symbol-ref="ct1_membraneElasticity"/>
                <Symbol symbol-ref="ct1_membraneElasticity_edge"/>
                <Symbol symbol-ref="ct2_adhesion"/>
                <Symbol symbol-ref="ct2_membrane_str"/>
                <Symbol symbol-ref="ct2_area"/>
                <Symbol symbol-ref="ct2_area_edge"/>
                <Symbol symbol-ref="ct2_membraneElasticity"/>
                <Symbol symbol-ref="ct2_membraneElasticity_edge"/>
            </Input>
            <Output>
                <TextOutput file-numbering="time" file-name="cell_membrane_properties"/>
            </Output>
            <!--    <Disabled>
        <Plots>
            <Plot time-step="1">
                <Style grid="true" style="linespoints"/>
                <Terminal terminal="png"/>
                <X-axis>
                    <Symbol symbol-ref="time"/>
                </X-axis>
                <Y-axis>
                    <Symbol symbol-ref="ct1_adhesion"/>
                    <Symbol symbol-ref="ct2_adhesion"/>
                </Y-axis>
            </Plot>
        </Plots>
    </Disabled>
-->
        </Logger>
        <!--    <Disabled>
        <DisplacementTracker time-step="1" celltype="ct1"/>
    </Disabled>
-->
        <!--    <Disabled>
        <DisplacementTracker time-step="1" celltype="ct2"/>
    </Disabled>
-->
        <ModelGraph format="dot" reduced="true" include-tags="#untagged"/>
        <Logger time-step="1">
            <Input/>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="-1">
                    <Style style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="time"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="celltype.ct1.size"/>
                        <Symbol symbol-ref="celltype.ct2.size"/>
                    </Y-axis>
                </Plot>
            </Plots>
        </Logger>
    </Analysis>
</MorpheusModel>
